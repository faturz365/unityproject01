using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KodinganUntukSpawn : MonoBehaviour
{
    public Transform[] spawnpoints;
    public GameObject[] enemyPrefabs;
    public float BesarObjek = 0.1f;
    public int benar = 0;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(spawnenemy());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    IEnumerator spawnenemy()
    {
        while (benar < 10)
        {
            int randEnemy = Random.Range(0, enemyPrefabs.Length);
            int randSpawPoint = Random.Range(0, spawnpoints.Length);
            GameObject newobject = Instantiate(enemyPrefabs[0], spawnpoints[randSpawPoint].position, transform.rotation);
            newobject.transform.localScale = new Vector3(BesarObjek, BesarObjek, BesarObjek);
            yield return new WaitForSeconds(2);
        }
     }
    
}
