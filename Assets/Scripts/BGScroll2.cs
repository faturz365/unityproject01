using UnityEngine;

public class BGScroll2 : MonoBehaviour
{
    [Range(-1f, 1f)]
    public float movingspeed = 0.8f;
    private float offset;
    private Material mat;

    void Start()
    {
        mat = GetComponent<Renderer>().material;

    }

    // Update is called once per frame
    void Update()
    {
        offset += (Time.deltaTime * movingspeed) / 10f;
        mat.SetTextureOffset("_MainTex", new Vector2(0, offset));
    }
}