using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContohGerak : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform player;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Vector3 target = Vector3.Lerp(gameObject.transform.position, player.position, 0.5f * Time.deltaTime);

        transform.position = Vector3.Lerp(gameObject.transform.position, player.position, 0.5f * Time.deltaTime);

    }

}
