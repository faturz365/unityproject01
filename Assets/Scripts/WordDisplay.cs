using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WordDisplay : MonoBehaviour
{

	public Text text;
	public float fallSpeed = 1f;
	public Transform player;
	public SpawnMissiletest misil;



	public void Start()
	{
		player = GameObject.Find("Player").GetComponent<Transform>();
    }
    public void SetWord(string word)
	{
		text.text = word;
	}

	public void RemoveLetter()
	{
        text.text = text.text.Remove(0, 1);
		text.color = Color.green;
	}

	public void RemoveWord()
	{
		misil.spawnmisil(gameObject.transform);
        SoundManager.playsound("tembak");
        
		//Destroy(gameObject);

	}

	private void Update()
	{
		//transform.Translate(0f, -fallSpeed * Time.deltaTime, 0f);
		//transform.position = Vector3z.Lerp(gameObject.transform.position, player.position, fallSpeed * Time.deltaTime);
		transform.position = Vector3.MoveTowards(gameObject.transform.position, player.position, fallSpeed * Time.deltaTime);
	}
   
}
