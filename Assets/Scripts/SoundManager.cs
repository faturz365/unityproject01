using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class SoundManager : MonoBehaviour
{

    public static AudioClip bomb, launchattack,UImenuclick,UIgameclik,laser,ream ;
    static AudioSource audioSrc;
    [SerializeField] Image soundonicon;
    [SerializeField] Image soundofficon;
    private bool muted = false;


    // Start is called before the first frame update
   public void Start()
    {
        bomb = Resources.Load<AudioClip>("bomb_01");
        launchattack = Resources.Load<AudioClip>("retro_laser_gun_shoot_30");
        UImenuclick = Resources.Load<AudioClip>("ui_menu_sound");
        UIgameclik = Resources.Load<AudioClip>("BUTTON_Plastic_Light_Switch_Off_mono");
        laser = Resources.Load<AudioClip>("Laser_cast");
        ream = Resources.Load<AudioClip>("Rearm_cast");

        audioSrc = GetComponent<AudioSource>();

        if (!PlayerPrefs.HasKey("muted"))
        {
            PlayerPrefs.SetInt("muted", 0);
            LoadMute();
        }
        else
        {
            LoadMute();
        }
        perubahanicon();
        AudioListener.pause = muted;

    }

    public void onbuttonditekan()
    {
        if (muted == false)
        {
            muted = true;
          //  AudioListener.volume = 0f;
            AudioListener.pause = true;
        }
        else 
        {
            muted = false;
            //AudioListener.volume = 1f;
            AudioListener.pause = false;
        }
        SaveMute();
        perubahanicon();
    }

    private void LoadMute()
    {
        muted = PlayerPrefs.GetInt("muted") == 1;
    }
    private void SaveMute()
    {
        PlayerPrefs.SetInt("muted", muted ? 1 : 0);
    }

    private void perubahanicon()
    {
        if (muted == false)
        {
            soundonicon.enabled = true;
            soundofficon.enabled = false;
        }
        else
        {
            soundonicon.enabled = false;
            soundofficon.enabled = true;
        }
    }

    public static void playsound(string clip)
    {

        switch (clip)
        {

            case "duar":
                audioSrc.PlayOneShot(bomb);
                break;

            case "tembak":
                audioSrc.PlayOneShot(launchattack);
                break;

            case "UIMenu":
                audioSrc.PlayOneShot(UImenuclick);
                break;

            case "UIMenu1":
                audioSrc.PlayOneShot(UIgameclik);
                break;
            case "laser":
                audioSrc.PlayOneShot(laser);
                break;
            case "ream":
                audioSrc.PlayOneShot(ream);
                break;

        }
    }
}
