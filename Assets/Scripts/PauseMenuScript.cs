using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



public class PauseMenuScript : MonoBehaviour
{
    [SerializeField]
    public GameObject PauseMenu;
    public GameObject UI_Text;

    public void pausegame()
    {
        SoundManager.playsound("UIMenu1");
        Time.timeScale = 0f;
        PauseMenu.SetActive(true);
    }
   
    public void resumegame()
    {
        SoundManager.playsound("UIMenu1");
        Time.timeScale = 1f;
        PauseMenu.SetActive(false); 
    }

    public void homebutton()
    {
        SoundManager.playsound("UIMenu1");
        PauseMenu.SetActive(false);
        Time.timeScale = 1f;
        SceneManager.LoadScene("Menu");
    }

    public void retrybutton()
    {
        SoundManager.playsound("UIMenu1");
        PauseMenu.SetActive(false);
        Time.timeScale = 1f;
        SceneManager.LoadScene("Main");
    }

}
