using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public Text HighScore;
    int highscore;
    bool mulai = false;
    [SerializeField]
    private float waktuberjalan = 0f;
    private float delaykemain =0.5f;
    // Start is called before the first frame update
    public void Playgame()
    {
        mulai = true;
      
    }
    public void quitgame()
    {
        Debug.Log("Quit");
        Application.Quit();
    }
    public void Start()
    {
        HighScore.text = "HighScore :" + PlayerPrefs.GetInt("Highscore");
        
    }

    private void Update()
    {
        if (mulai)
        {
            waktuberjalan += Time.deltaTime;

            if (waktuberjalan > delaykemain)
            {
                SceneManager.LoadScene("Main");
            }
        }
    }
}
