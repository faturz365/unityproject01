using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[RequireComponent(typeof(Rigidbody2D))]
public class Horming : MonoBehaviour
{
    public Transform target;
	public GameObject ledakan;
	public float speed = 3f;
	public float rotateSpeed = 200f;

	private Rigidbody2D rb;
	public WordDisplay thaword;
    public ScoreManager addingscore;

	int energy = 0;
	// Use this for initialization
	void Start()
	{
		rb = GetComponent<Rigidbody2D>();
        addingscore = GameObject.Find("Canvas").GetComponent<ScoreManager>();
		
	}

	void FixedUpdate()
	{
		Vector2 direction = (Vector2)target.position - rb.position;
		direction.Normalize();
		float rotateAmount = Vector3.Cross(direction, transform.up).z;
		rb.angularVelocity = -rotateSpeed * rotateAmount;
		rb.velocity = transform.up * speed;
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		//Debug.Log(col.name);
		if (col.tag == "thaword" && col.transform == target.transform)
		{
			Instantiate(ledakan, transform.position, Quaternion.identity);
			Destroy(gameObject);
			Destroy(target.gameObject);
			addingscore.tambahscore();
			SoundManager.playsound("duar");
			//Debug.Log(PlayerPrefs.GetInt("energy"));
			energy = PlayerPrefs.GetInt("energy");
			if (energy >= 100)
            {
				energy = 100;
            }
            else
            {
				energy = 5 + PlayerPrefs.GetInt("energy");
				PlayerPrefs.SetInt("energy", energy);
			}
		}
	}
    /*private void OnDestroy(GameObject target.gameObject)
    {
        OnDestroy(target.gameObject);
        addingscore.tambahscore();
	}*/
}



