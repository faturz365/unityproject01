using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LeserBeamScript : MonoBehaviour
{
    public GameObject leserbeam;
    public Transform player;
    public GameObject buttontembak;
    public Text textEnergy;

    public WordManager woman;
    public WordDisplay dissplay;
    public Word werd;
    public ScoreManager addingscore;
    // Start is called before the first frame update
    public int energy = 0;

    private bool isfull = false;
     
    public void onTapLaser()
    {
        Instantiate(leserbeam, player.transform.position, Quaternion.Euler(0f, 0f, 90f));
        SoundManager.playsound("laser");
        woman.words.Clear();
        var induk = GameObject.FindGameObjectsWithTag("thaword");
        woman.hasActiveWord = false;
        woman.activeWord = null;
        dissplay.text.text = null;
        werd.typeIndex = 0;
        for(int i = 0;i < induk.Length; i++)
        {
            Destroy(induk[i]);
        }
        PlayerPrefs.SetInt("energy", 0);
        addingscore.tambahscorebeam();
    }
    private void Update()
    {
        
        energy = PlayerPrefs.GetInt("energy");
        textEnergy.text = "Laser Energy " +"\n"+ energy.ToString() +"%";
        if (energy >= 100)
        {
            buttontembak.SetActive(true);
            if (isfull == false) 
            {
                SoundManager.playsound("ream");
                isfull = true;
            }
        }
        else
        {
            buttontembak.SetActive(false);
            isfull = false;
            
        } 
    }
    private void Start()
    {
        PlayerPrefs.SetInt("energy", 0);
        addingscore = GameObject.Find("Canvas").GetComponent<ScoreManager>();
    }
}
