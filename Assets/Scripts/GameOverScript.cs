using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverScript : MonoBehaviour
{
    public Text Text_Score;
    public Text Text_Highscore;
    // Start is called before the first frame update
    int Highscore = 0;
    int Score = 0;
    void Start()
    {
        Highscore = PlayerPrefs.GetInt("Highscore");
        Score = PlayerPrefs.GetInt("scoresementara");
        Text_Highscore.text = "Highscore :" + Highscore.ToString();
        Text_Score.text = "Score : " + Score.ToString();
        
    }


    public void home()
    {
        SceneManager.LoadScene("Menu");

    }
    public void retry()
    {
        SceneManager.LoadScene("Main");
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
