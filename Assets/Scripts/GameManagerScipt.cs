using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManagerScipt : MonoBehaviour
{
    Scene scenesaatini;
    // Start is called before the first frame update
    void Start()
    {
        Scene scenesaatini = SceneManager.GetActiveScene();

        if (scenesaatini.name == "Menu") 
        {
            PlayerPrefs.SetInt("level", 1);
        }
     
        else if (scenesaatini.name == "Main")
        {
            PlayerPrefs.SetInt("level", 1);
        }
    
        else if (scenesaatini.name == "GameOver")
        {
            PlayerPrefs.SetInt("level", 0);
        }


    }

    // Update is called once per frame
    void Update()
    {
        if (PlayerPrefs.GetInt("scoreuntuklevel") >= 0 && PlayerPrefs.GetInt("scoreuntuklevel") <= 1000)
        {
            PlayerPrefs.SetInt("level", 1);
            PlayerPrefs.SetFloat("waktudelay", 3.5f);
        }
       else if (PlayerPrefs.GetInt("scoreuntuklevel") > 1000 && PlayerPrefs.GetInt("scoreuntuklevel") <= 2000)
        {
            PlayerPrefs.SetInt("level", 2);
            PlayerPrefs.SetFloat("waktudelay", 3f);
        }
       else if (PlayerPrefs.GetInt("scoreuntuklevel") > 2000 && PlayerPrefs.GetInt("scoreuntuklevel") <= 3000)
        {
            PlayerPrefs.SetInt("level", 3);
            PlayerPrefs.SetFloat("waktudelay", 2.5f);
        }
        else if (PlayerPrefs.GetInt("scoreuntuklevel") > 4000 && PlayerPrefs.GetInt("scoreuntuklevel") <= 5000)
        {
            PlayerPrefs.SetInt("level", 4);
            PlayerPrefs.SetFloat("waktudelay", 2f);
        }
        else if (PlayerPrefs.GetInt("scoreuntuklevel") > 5000 && PlayerPrefs.GetInt("scoreuntuklevel") <= 6000)
        {
            PlayerPrefs.SetInt("level", 5);
            PlayerPrefs.SetFloat("waktudelay", 1.5f);
        }
        else if (PlayerPrefs.GetInt("scoreuntuklevel") > 6000 )
        {
            PlayerPrefs.SetInt("level", 6);
            PlayerPrefs.SetFloat("waktudelay", 1.5f);
        }
    }
}
