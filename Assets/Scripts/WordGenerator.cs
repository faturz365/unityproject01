using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WordGenerator : MonoBehaviour
{
		public static string GetRandomWord()

		{
		string randomWord ="";
		int levelini = PlayerPrefs.GetInt("level");
		string[,] wordList = new string[,] {
			{ "ebi", "iga", "oli", "ubi", "dua", "ada","ayo" },
			{ "buku","kita","lupa","taro","alam","sapu","nila" },
			{ "kulit","hilal","hutan","bambu","kamar","busuk","ceria" },
			{ "begini","lombok","mangga","pisang","lengan","ganggu","luapan" },
			{ "bahagia","sahabat","kenangan","tangisan","bintang","semesta","bingung" }
			};
		int selectedIndex = Random.Range(0, 6);
		Debug.Log(levelini);
		if (PlayerPrefs.GetInt("level") <= 5)
        {
			 randomWord = wordList[levelini - 1, selectedIndex];
		}
        else if(PlayerPrefs.GetInt("level") > 5 )
		{
			 randomWord = wordList[(int)Random.Range(0,4), selectedIndex];

		}
		return randomWord;
		}

}
