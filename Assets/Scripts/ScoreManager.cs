using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public Text Scoretext;
    public Text HighScore;
    

   public int score = 0;
    int highscore = 0;

    // Start is called before the first frame update
    void Start()
    {
        highscore = PlayerPrefs.GetInt("Highscore", 0);
        Scoretext.text = "" + score.ToString();
        HighScore.text = "Highscore :" +"\n"+ highscore.ToString();

    }

    // Update is called once per frame
    public void tambahscore()
    {
        score += 100;
        Scoretext.text = "" + score.ToString();
        PlayerPrefs.SetInt("scoreuntuklevel", score);
        if (highscore < score )
        {
            PlayerPrefs.SetInt("Highscore", score);
        }
    }

    public void tambahscorebeam()
    {
        score += 500;
        Scoretext.text = "" + score.ToString();
        PlayerPrefs.SetInt("scoreuntuklevel", score);
        if (highscore < score)
        {
            PlayerPrefs.SetInt("Highscore", score);
        }
    }
}

    
