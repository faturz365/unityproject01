using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class ColiisionProgram : MonoBehaviour
{
    [SerializeField] Image heart1;
    [SerializeField] Image heart2;
    [SerializeField] Image heart3;
    public Text texthp;
    public int hp = 3;
    public WordManager woman;
    public WordDisplay dissplay;
    public Word werd;
    public ScoreManager score;
    public void Start()
    {
        texthp.text = " HP = " + hp.ToString();
    }
    private void OnTriggerEnter2D(Collider2D col)
    { 
       if (col.tag == "thaword")
        {
            if (woman.hasActiveWord == true)
            {
                //woman.words.Remove(woman.activeWord);
                woman.words.RemoveAt(index: 0);
                woman.hasActiveWord = false;
                woman.activeWord = null;
                dissplay.text.text = null;
                werd.typeIndex = 0;
                Destroy(col.gameObject);
            }
            else
            {
                woman.words.RemoveAt(index: 0);
                Destroy(col.gameObject);
            }
            if (hp > 1)
            {
                
                hp -= 1;
                texthp.text = " HP = " + hp.ToString();
                hpimage();
            }
            else {
                hpimage();
                PlayerPrefs.SetInt("scoresementara",score.score);
                SceneManager.LoadScene("GameOver");
            }
        }

    }
    private void hpimage()
    {
        if(hp == 3)
        {
            heart1.enabled = true;
            heart2.enabled = true;
            heart3.enabled = true;
        }
        else if (hp == 2)
            {
                heart1.enabled = true;
                heart2.enabled = true;
                heart3.enabled = false;
            }
        else if (hp == 1)
        {
            heart1.enabled = true;
            heart2.enabled = false;
            heart3.enabled = false;
        }
        else 
        {
            heart1.enabled = false;
            heart2.enabled = false;
            heart3.enabled = false;
        }
    }
   
}
