using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGmusicScript : MonoBehaviour
{

    private static BGmusicScript BGmusic;

    private void Awake()
    {
          if (BGmusic == null)
        {
            BGmusic = this;
            DontDestroyOnLoad(BGmusic);
        }
        else
        {
            Destroy(gameObject);
        }
    }


}
