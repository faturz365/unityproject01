using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WordSpawner : MonoBehaviour
{

	public GameObject wordPrefab;
	public Transform wordCanvas;
	public SpawnMissiletest misil;

	public WordDisplay SpawnWord()
	{

		Vector3 randomPosition = new Vector3(Random.Range(-2.5f, 2.5f), 5.5f,0f);
		GameObject wordObj = Instantiate(wordPrefab, randomPosition, Quaternion.identity, wordCanvas);
		//wordObj.transform.position = new Vector3(wordObj.transform.position.x, wordObj.transform.position.y, 0f);
		WordDisplay wordDisplay = wordObj.GetComponent<WordDisplay>();
        wordDisplay.misil = misil;
		wordObj.tag = "thaword";

		return wordDisplay;
	}

}
